import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAg6-hG7pLZciKvqfHwGK1hzJe7pAXoRSE",
  authDomain: "mymoney-4d17e.firebaseapp.com",
  projectId: "mymoney-4d17e",
  storageBucket: "mymoney-4d17e.appspot.com",
  messagingSenderId: "1015043939729",
  appId: "1:1015043939729:web:9626d63a47b76c1bc9ce30",
};

initializeApp(firebaseConfig);

const db = getFirestore();
export { db };
