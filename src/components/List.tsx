import { Sub } from "../types";

interface Props {
  subs: Array<Sub>;
}

export default function List(props: Props) {
  const { subs } = props;
  return (
    <ul>
      {subs.map((sub) => (
        <li key={sub.nick}>
          <img src={sub.avatar} />
          <h4>
            {sub.nick} <small>{sub.subMonths}</small>
          </h4>
        </li>
      ))}
    </ul>
  );
}
