import { BrowserRouter, Link, Route, Routes } from "react-router-dom";
import Home from "./pages/home/Home.page";
import Signin from "./pages/signin/Signin.page";
import Signup from "./pages/signup/Signup.page";

import { db } from "./firebase/config";
import { collection, getDocs } from "firebase/firestore";

import { useState, useRef, useEffect } from "react";
import List from "./components/List";
import Form from "./components/Form";
import "./App.css";
import { Sub } from "./types";

function App() {
  const [subs, setSubs] = useState<Sub[]>(INITIAL_STATE);
  const divRef = useRef<HTMLDivElement>(null);

  const handleNewSub = (newSub: Sub): void => {
    setSubs((subs) => [...subs, newSub]);
  };

  return (
    <div className="App" ref={divRef}>
      <BrowserRouter>
        <nav>
          <h1>My money</h1>
          <Link to="/">Home</Link>
          <Link to="/signin">Sign in</Link>
          <Link to="/signup">Sign up</Link>
        </nav>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/signin" element={<Signin />} />
          <Route path="/signup" element={<Signup />} />
        </Routes>
      </BrowserRouter>
      {/* <h1>subs</h1>
      <List subs={subs} />
      <Form onNewSub={handleNewSub} /> */}
    </div>
  );
}

const INITIAL_STATE = [
  {
    nick: "ceci",
    subMonths: 3,
    avatar: "https://i.pravatar.cc/150?u=ceci",
    description: "Ceci k ase",
  },
  {
    nick: "nacho",
    subMonths: 3,
    avatar: "https://i.pravatar.cc/150?u=nachete",
    description: "lo k kiera",
  },
];

export default App;
